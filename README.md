# Deploy GitLab Helm Chart on GKE

Script is designed to deploy GitLab HEML charts on GKE from Ubuntu.  
Can be used on other platforms as long as KUBECTL and HELM are already installed.  
See optional steps below for starting a Ubuntu Instance on GCP.

## Clone GKE Script
`git clone https://gitlab.com/mpower/gitlab-gke-demo.git`

## Update Config
Check the values in the `/defaults` file and adjust as needed. 

## Run Deploy Script
`./deploy`  
To run silently use `./deploy -s`

## Optional Steps
These steps will start a Ubuntu compute instance VM on GCP used to deploy your GitLab Helm cluster.  
Recommended if you don't have KUBECTL and HELM locally. Google Cloud SDK must be installed locally to run gcloud commands.

### Start Development Instance
Replace `<PROJECT>`, `<SERVICE-ACCOUNT>`, and `<ZONE>` with your own values  
see [Authenticating to Cloud Platform with Service Accounts](https://cloud.google.com/kubernetes-engine/docs/tutorials/authenticating-to-cloud-platform)

`gcloud compute instances create dev01 --project=<PROJECT> --service-account=<SERVICE-ACCOUNT> --zone=<ZONE> --machine-type f1-micro --subnet default --image-project ubuntu-os-cloud --boot-disk-size 30GB --boot-disk-type pd-standard --boot-disk-device-name dev01 --image ubuntu-1804-bionic-v20181222 --network-tier=PREMIUM --maintenance-policy=MIGRATE --scopes=https://www.googleapis.com/auth/cloud-platform`

### Connect to Development Instance
`gcloud compute ssh dev01`

### Init GCloud
`gcloud init`

## After Deployment

## Check Cluster Deployment Status
`helm status gitlab`

## Get GitLab root Password
`./deploy -p`

## BUI
`https://gitlab.<DOMAIN>`

# Troubleshooting
`kubectl logs <POD>`

`kubectl attach <POD>`

# Teardown
`./deploy -k`

## Terminate Development Instance
`gcloud compute instances delete dev01`
