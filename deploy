#!/usr/bin/env bash

########################
# Deploy GitLab on GKE #
########################

if [[ ! -v CI_JOB_ID ]]; then
    source defaults
fi

usage ()
{
    echo "Usage:  $(basename $0) options: [-s SILENT] [-k KILL] [-p GL ROOT PW]"
}

reserve_new_ip () {
    gcloud beta compute addresses create ${CLUSTER_NAME} --project="$PROJECT" --region="$REGION" --network-tier=PREMIUM
}

get_external_ip (){
    gcloud compute addresses list --project="$PROJECT" --filter="status=RESERVED" --format="value(address)"
}

get_gl_ip () {
    gcloud compute addresses list --project="$PROJECT" --filter="name=$CLUSTER_NAME" --format="value(address)"
}

setup_dns_records () {
    gcloud beta dns managed-zones create ${DNS_ZONE} --dns-name="$FQDN" --visibility=public --description="$CLUSTER_NAME Zone"
    gcloud dns record-sets transaction start --zone="$DNS_ZONE" --project="$PROJECT"
    gcloud dns record-sets transaction add ${IP} --name=\*.${FQDN}. --ttl=300 --type=A --zone="$DNS_ZONE" --project="$PROJECT"
    gcloud dns record-sets transaction execute --zone="$DNS_ZONE" --project="$PROJECT"
}

start_kube_cluster () {
    gcloud beta container clusters create "$CLUSTER_NAME" --cluster-version="$CLUSTER_VERSION" --num-nodes="$NODES" --min-nodes="$NODES_MIN" --max-nodes="$NODES_MAX" --machine-type="$MACHINE_TYPE" --image-type="COS" --disk-type="pd-standard" --disk-size="$DISK_SIZE" --project="$PROJECT" --zone="$ZONE" --username="admin" --scopes="https://www.googleapis.com/auth/compute","https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --enable-cloud-logging --enable-cloud-monitoring --no-enable-ip-alias --network "projects/$PROJECT/global/networks/default" --subnetwork "projects/$PROJECT/regions/$REGION/subnetworks/default" --enable-autoscaling --addons HorizontalPodAutoscaling,HttpLoadBalancing,KubernetesDashboard --enable-autoupgrade --enable-autorepair
    gcloud container clusters get-credentials "$CLUSTER_NAME" --zone="$ZONE" --project="$PROJECT"
}

deploy_helm_charts (){
    kubectl create -f rbac-config.yaml
    helm init --service-account tiller
    sleep 5
    helm repo add gitlab https://charts.gitlab.io/
    helm repo update
    echo "Preparing to deploy HELM Chart"
    sleep 10
    if [[ "$GITLAB_CE" = false ]]; then
        helm upgrade --install gitlab gitlab/gitlab --timeout 600 --set global.hosts.domain=${FQDN} --set global.hosts.externalIP=${IP} --set certmanager-issuer.email=${EMAIL} --set gitlab.migrations.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-rails-ce --set gitlab.sidekiq.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-sidekiq-ce --set gitlab.unicorn.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-unicorn-ce --set gitlab.unicorn.workhorse.image=registry.gitlab.com/gitlab-org/build/cng/gitlab-workhorse-ce --set gitlab.task-runner.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-task-runner-ce
    else
        helm upgrade --install gitlab gitlab/gitlab --timeout 600 --set global.hosts.domain=${FQDN} --set global.hosts.externalIP=${IP} --set certmanager-issuer.email=${EMAIL}
    fi
}

get_gitlab_password () {
    ROOT_PASSWORD=$(kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath={.data.password} | base64 --decode)
}

teardown_cluster () {
    IP=$(get_gl_ip)
    helm delete gitlab
    helm delete gitlab --purge
    gcloud container clusters delete ${CLUSTER_NAME} --project="$PROJECT" --zone="$ZONE" --quiet
    gcloud compute addresses delete ${CLUSTER_NAME} --project="$PROJECT" --region="$REGION" --quiet
    gcloud dns record-sets transaction start --zone="$DNS_ZONE" --project="$PROJECT"
    gcloud dns record-sets transaction remove ${IP} --name=\*.${FQDN}. --ttl=300 --type=A --zone="$DNS_ZONE" --project="$PROJECT"
    gcloud dns record-sets transaction execute --zone="$DNS_ZONE" --project="$PROJECT"
    gcloud dns managed-zones delete ${DNS_ZONE}
    gcloud compute instances delete $(gcloud compute instances list --filter="name:gke-$CLUSTER_NAME*" --format="value(name)") --zone="$ZONE" --quiet
    gcloud compute disks delete $(gcloud compute disks list --filter="name:gke-$CLUSTER_NAME*" --format="value(name)") --zone="$ZONE" --quiet
}

while getopts ":skpi" opt; do
  case ${opt} in
        s)
            SILENT=true ;;
        k)
            teardown_cluster
            exit 0
            ;;
        p)
            get_gitlab_password
            printf "Your GitLab root password is:\n${ROOT_PASSWORD}\nLogin at https://gitlab.${FQDN}\n"
            exit 0
            ;;
        i)
            IP=$(get_gl_ip)
            printf "Your GitLab IP is: ${IP}\n"
            exit 0
            ;;
        \?)
            usage
            exit 0
             ;;
    esac
done

# Set Text Colours
GREEN_TEXT="\033[1;32m"
RED_TEXT="\033[1;31m"
BLUE_TEXT="\033[1;34m"
PLAIN_TEXT="\033[0m"

# Install Dependency's
if [[ "$SILENT" = false ]]; then
    printf "${BLUE_TEXT}Check KUBECTL/HELM${PLAIN_TEXT}\n"
    printf "${RED_TEXT}When ready, Press [ENTER] to continue ...${PLAIN_TEXT}\n"
    read READY
fi

if ! [[ -x "$(command -v kubectl)" ]]; then
    printf "${BLUE_TEXT}KUBECTL Not Found. Installing!${PLAIN_TEXT}\n"
    sudo snap install kubectl --classic
fi

if ! [[ -x "$(command -v helm)" ]]; then
    printf "${BLUE_TEXT}HELM Not Found. Installing!${PLAIN_TEXT}\n"
    sudo snap install helm --classic
fi

if [[ "$SILENT" = false ]]; then
    printf "${GREEN_TEXT}DONE: KUBECTL/HELM${PLAIN_TEXT}\n\n"
fi
# DONE: KUBE/HELM
sleep 1

# Get IP Address
if [[ "$SILENT" = false ]]; then
    printf "${BLUE_TEXT}Reserve IP Address${PLAIN_TEXT}\n"
    printf "${RED_TEXT}Press [ENTER] to automatically reserve a new IP or [N] to use an existing IP${PLAIN_TEXT}\n"
    read RESERVE_IP
fi

if [[ "$RESERVE_IP" = "N" ]] || [[ "$RESERVE_IP" = "n" ]]; then
    IP=$(get_external_ip)
else
    reserve_new_ip
    IP=$(get_external_ip)
fi

if [[ "$SILENT" = false ]]; then
    printf "${GREEN_TEXT}DONE: IP Address is ${IP}${PLAIN_TEXT}\n\n"
fi
# DONE: IP Address
sleep 1

# Setup DNS
if [[ "$SILENT" = false ]]; then
    printf "${BLUE_TEXT}Auto Configure DNS?${PLAIN_TEXT}\n"
    printf "${RED_TEXT}Press [ENTER] to automatically configure DNS or [N] to skip${PLAIN_TEXT}\n"
    read SETUP_DNS
fi

if [[ "$SETUP_DNS" = "N" ]] || [[ "$SETUP_DNS" = "n" ]]; then
    printf "${GREEN_TEXT}Skipping DNS Setup${PLAIN_TEXT}\n"
else
    setup_dns_records
fi

if [[ "$SILENT" = false ]]; then
    printf "${GREEN_TEXT}DONE: DNS record added for ${FQDN}${PLAIN_TEXT}\n\n"
fi
# DONE: DNS Setup
sleep 1

# Start Kubernetes Cluster
if [[ "$SILENT" = false ]]; then
    printf "${BLUE_TEXT}Start GKE Cluster${PLAIN_TEXT}\n"
    printf "${RED_TEXT}When ready, Press [ENTER] to continue ...${PLAIN_TEXT}\n"
    read READY
fi

start_kube_cluster

if [[ "$SILENT" = false ]]; then
    printf "${GREEN_TEXT}DONE: GKE Cluster${PLAIN_TEXT}\n\n"
fi
# DONE: STARTING CLUSTER
sleep 1

# Configure HELM
if [[ "$SILENT" = false ]]; then
    printf "${BLUE_TEXT}Configure Helm${PLAIN_TEXT}\n"
    printf "${RED_TEXT}When ready, Press [ENTER] to continue ...${PLAIN_TEXT}\n"
    read READY
fi

deploy_helm_charts

if [[ "$SILENT" = false ]]; then
    get_gitlab_password
    printf "${GREEN_TEXT}DONE: GitLab deployed using HELM Chart to GKE!${PLAIN_TEXT}\nRun \"helm status gitlab\" to view deployment status.\nYour GitLab root password is:\n${ROOT_PASSWORD}\nLogin at https://gitlab.${FQDN}\n"
fi
# DONE: HELM Deploy

exit 0
